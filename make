#!/bin/bash

function nasm_compile
{
    nasm -I ./source/ -felf64 ./source/$1.asm -o ./temp/$1.o
}

nasm_compile rc4

gcc -O3 ./temp/*.o ./test/test.c -o test.out