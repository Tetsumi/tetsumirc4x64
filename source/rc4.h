#include <stdint.h>

typedef enum
{
	ERROR_NONE,
	ERROR_UNKNOWN,
	ERROR_NULL
} RC4_Error;

typedef uint8_t     RC4_Context[258];

RC4_Error RC4_init    (RC4_Context c, const uint8_t const *k, size_t length);
RC4_Error RC4_encrypt (RC4_Context c,
		       uint8_t * const buffer,
		       size_t length);
RC4_Error RC4_decrypt (RC4_Context c,
		       uint8_t * const buffer,
		       size_t length);
