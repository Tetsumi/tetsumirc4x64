;;;
;;;   tetsumirc4x64
;;;
;;;   Copyright 2015 Tetsumi <tetsumi@vmail.me>
;;; 
;;;   This program is free software: you can redistribute it and/or modify
;;;   it under the terms of the GNU General Public License as published by
;;;   the Free Software Foundation, either version 3 of the License, or
;;;   (at your option) any later version.
;;;
;;;   This program is distributed in the hope that it will be useful,
;;;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;   GNU General Public License for more details.
;;;
;;;   You should have received a copy of the GNU General Public License
;;;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;; 	
global RC4_init
global RC4_encrypt
global RC4_decrypt

	; Error codes
	ERROR_NONE    equ 0
	ERROR_UNKNOWN equ 1
	ERROR_NULL    equ 2

section .text

;;; [PUBLIC]
;;; Arguments
;;;   rdi = context
;;;   rsi = key
;;;   rdx = key's length
;;; Returns
;;;   rax = error code
;;; Notes
;;;   Takes advantage of overflow for control flow.
RC4_init:
	test rdi, rdi
	jz return_ERROR_NULL
	test rsi, rsi
	jz return_ERROR_NULL
	push rbx
	mov word [rdi], 0
	lea rdi, [rdi + 2]
	mov rax, 0x0706050403020100
	mov rbx, 0x0808080808080808
	mov rcx, 32
.loop_init:
	mov [rdi], rax
	add rax, rbx
	lea rdi, [rdi + 8]
	sub rcx, 1
	jnz .loop_init
	lea rdi, [rdi - (8 * 32)]
	xor r9, r9
	xor r8, r8
	xor rbx, rbx
.loop_key:
	mov al, [rdi + rcx]
	add r8b, al
	cmp rbx, rdx
	cmove rbx, r9
	add r8b, [rsi + rbx]
	mov r10b, [rdi + r8]
	mov [rdi + rcx], r10b
	mov [rdi + r8], al
	add rbx, 1
	add cl, 1
	jnz .loop_key
	pop rbx
	jmp return_ERROR_NONE
	
;;; [PUBLIC]
;;; Arguments
;;;   rdi = context
;;;   rsi = buffer
;;;   rdx = length
;;; Returns
;;;   rax = error code	
RC4_decrypt:
RC4_encrypt:
	lea rsi, [rdx + rsi]
	neg rdx
	movzx eax, byte [rdi]
	movzx ecx, byte [rdi + 1]
	add rdi, 2
	push rbx
	xor rbx, rbx
.loop:
	add eax, 1
	movzx eax, al
	movzx ebx, byte [rdi + rax]
	add cl, bl
	movzx r8, byte [rdi + rcx]
	mov [rdi + rcx], bl
	add ebx, r8d
	mov [rdi + rax], r8b
	movzx ebx, bl
	movzx ebx, byte [rdi + rbx]
	xor [rsi + rdx], bl
	add rdx, 1
	jnz .loop	
	mov [rdi - 1], cl
	mov [rdi - 2], al
	pop rbx
	jmp return_ERROR_NONE

;;; [PRIVATE]
return_ERROR_NONE:
	mov rax, ERROR_NONE
	ret

;;; [PRIVATE]
return_ERROR_UNKNOWN:
	mov rax, ERROR_UNKNOWN
	ret

;;; [PRIVATE]
return_ERROR_NULL:
	mov rax, ERROR_NULL
	ret
