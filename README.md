tetsumirc4x64
===================

Implementation of RC4 written in NASM x64 assembly. Includes a benchmark.

![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png) 

[Note: I am no longer working on this.]